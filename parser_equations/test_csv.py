import parser_equations as PE
import csv
import sys
import os
sys.path.insert(1, '../parser_docs')
import  parser_documents

def remove_new_lines(list_of_equations):
    for i,eq in enumerate(list_of_equations):
        list_of_equations[i]=eq.replace("\n","")

def set_all_symbols(list_of_histograms):
    """ returns the set with all the symbols"""
    s=set()
    for histo in list_of_histograms:
        for symbol in histo.keys():
            s.add(symbol)
    return s                  

def print_txt_per_equation(filename, paper_identifier, set_of_symbols, list_of_histograms):
    """ Prints a txt (whitespace separated) file where the first row is the list of all the symbols.
        The first column contains the paperID-#equation. All the other entries contain the count of each symbol per equation."""
    with open(filename, "w") as output:
        output.write("paperID-#equation ")
        for symbol in set_of_symbols:
            output.write(symbol+" ")          
        output.write("\n")
        f=lambda sym, histo : histo[sym] if sym in histo.keys() else 0 
        for i,histo in enumerate(list_of_histograms):
            output.write(paper_identifier+"-"+str(i+1)+" ")
            for symbol in set_symbols:
                v=f(symbol, histo)
                output.write(str(v)+" ")
            output.write("\n")
            
    
#load dummy data
self_dir = os.path.dirname(os.path.realpath(__file__))
with open(self_dir + "/../parser_docs/all_tex_files/80;2012-12-21;math.gm.tex") as test_file:
        test_file = test_file.read()  

test_file = parser_documents.removeCustomCommands(test_file,1)
list_of_equations=parser_documents.getEquations(test_file, 1)

remove_new_lines(list_of_equations)

list_of_histograms=[]
for i in list_of_equations:
    eq=PE.Equation(i)
    list_of_histograms.append(eq.histogram)

set_symbols=set_all_symbols(list_of_histograms)
print(len(set_symbols))

print_txt_per_equation("../second.txt","test_paper", set_symbols, list_of_histograms)



    
# my_dict=eq.get_histogram()
# with open('mycsvfile.csv', 'w') as f:  
#     w = csv.DictWriter(f, my_dict.keys())
#     w.writeheader()
#     w.writerow(my_dict)

