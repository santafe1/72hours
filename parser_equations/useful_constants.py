OPERATOR_STOPPERS=("!","^","*","-","+","/","<",">","=","(",")","[","]","\\"," ","\t",\
                   "1","2","3","4","5","6","7","8","9","0",",","_","{","}", "&","$","|", ".",",", ":", ";","'","`",'"')
SYMBOLS_TO_SKIP=("_"," ","{","}","", "&","$", "%", "\n", '"',"'")

SYMBOLS_TO_REMOVE=("\\",""," ","\f","\r","\t", "begin", "end", "boldmath", "cal", "mathbb", "mathbf", "mathcal", "mathds",
"mathellipsis", "mathgroup", "mathit", "mathnormal", "mathrm", "mathscr", "mathsf", "mathtt", "left", "right", "text", "unboldmath",
                   "textrm", "quad", "qquad", "scriptsize", "small", "big", "Big", "notag", "textit", "underline", "overline","stackrel", "array", "pmatrix", "textbf", "textstyle", "textcolor", "cite", "nonumber")

SYMBOLS_TO_CHECK_IN=("mathrm", "operatorname")
