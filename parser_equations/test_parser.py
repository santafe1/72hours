import parser_equations as PE
import csv
import sys
import os

#s="\sin(x+y(3+z)^3)"
#s = "\alpha^2+\bm{2}"
#s=r"\dot{\bm{x}}_i = \bm{F}(\bm{x}_i) + \sigma \sum \limits_{j = 1}^n M_{ij} \bm{H}(\bm{x}_j), \quad i = 1,\dots,n,"
#s=r"\sin(\alpha+)"
#s="\sin(\alpha+)"
#s=r"{}".format(s)
# s=r"\x+"
#s=r"\begin{split}\nab_4\Hb+\tr X\Hb&=0, \qquad \nab_3 H + \tr \Xb H=0, \\ \ov{\tr X} \Hb + \tr X H&=0, \qquad   \ov{\tr \Xb} H + \tr \Xb \Hb=0,\\  \DDc\tr X +2\tr X\Hb&=0, \qquad  \DDc\tr \Xb +2\tr \Xb H=0. \label{eq:vanishing-relations-Kerr} \end{split}"
s=r"\sin(3+\mathrm{erfi}(3y^2))"



eq=PE.Equation(s)


print(eq.get_histogram())
# with open('mycsvfile.csv', 'w') as f:  
#     w = csv.DictWriter(f, my_dict.keys())
#     w.writeheader()
#     w.writerow(my_dict)

