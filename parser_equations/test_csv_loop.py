import parser_equations as PE
import csv
import sys
import os
sys.path.insert(1, '../parser_docs')
import parser_documents
from warnings import warn

def remove_new_lines(list_of_equations):
    for i,eq in enumerate(list_of_equations):
        list_of_equations[i]=eq.replace("\n","")

def set_all_symbols(list_of_histograms):
    """ returns the set with all the symbols"""
    s=set()
    for histo in list_of_histograms:
        for symbol in histo.keys():
            symbol.replace("'","")
            symbol.replace('"','')
            symbol.replace("%",'')
            s.add(symbol)
    return s

def print_csv_all_symbols(filename, set_of_symbols):
    with open(filename, "w") as output:
        output.write("paperID "+"year "+"category "+"equation# ")
        for symbol in set_of_symbols:
            output.write(symbol+" ")
        output.write("\n") 

def split_filename(filename):
    splitted=filename.split(";")
    index=splitted[0]
    year=splitted[1][0:4]
    category=splitted[2]
    return index, year, category
    
def print_csv_per_equation(filename, identifier, set_of_symbols, list_of_histograms):
    with open(filename, "a") as output:
        f=lambda sym, histo : histo[sym] if sym in histo.keys() else 0
        cum_number=0
        for filename, number in identifier:
            for i in range(number):
                index, year, category=split_filename(filename)
                output.write(index+" "+year+" "+category+" "+str(i+1)+" ")
                histo=list_of_histograms[cum_number]
                for symbol in set_symbols:
                    v=f(symbol, histo)
                    output.write(str(v)+" ")
                output.write("\n")
                cum_number+=1
                
        
def get_filename(path):
    rpath=path[::-1]
    filename=""
    for char in rpath:
        if char=="/":
            break
        filename+=char
    return filename[::-1]


self_dir = os.path.dirname(os.path.realpath(__file__))
#self_dir = "/Users/yuanzhao/Oscillator/72hours"
all_tex_files = os.scandir(self_dir + "/../parser_docs/all_tex_files/test/")
identifier =[]
list_of_equations=[]
for tex_file in all_tex_files:
    filename=get_filename(tex_file.path)
    with open(tex_file) as test_file:
        print(tex_file.path)
        test_file = test_file.read()
        try:
            test_file = test_file[test_file.index(r"\begin{document}"):]
            tmp=parser_documents.getEquations(test_file)
            remove_new_lines(tmp)
            list_of_equations.extend(tmp)
            identifier.append((filename,len(tmp)))
        except:
            warn("Couldn't find document beginning or document end!")

list_of_histograms=[]
for i in list_of_equations:
    eq=PE.Equation(i)
    ns=eq.number_symbols
    if ns!=0:
        list_of_histograms.append(eq.histogram)

set_symbols=set_all_symbols(list_of_histograms)
print("#symbols ",len(set_symbols)) 
print("#equations ",len(list_of_histograms))

print_csv_all_symbols("../test_many.txt", set_symbols)
print_csv_per_equation("../test_many.txt", identifier, set_symbols, list_of_histograms)




# my_dict=eq.get_histogram()
# with open('mycsvfile.csv', 'w') as f:
#     w = csv.DictWriter(f, my_dict.keys())
#     w.writeheader()
#     w.writerow(my_dict)
