from useful_constants import *

class CounterDictionary:
    def __init__(self):
        self.D=dict()

    def __str__(self):
        return str(self.D)

    def __repr__(self):
        return str(self.D)
    
    def __getitem__(self, key):
        return self.D[key]
    
    def add(self, name):
        if name in self.D:
            self.D[name]+=1
        else:
           self.D[name]=1

    def pop(self, key):
        if key in self.D:
            self.D.pop(key)
        else:
            pass
            

    def keys(self):
        return self.D.keys()

    def values(self):
        return self.D.values()

    

class Equation:

    def __init__(self,eq_string):
        self.equation_string=eq_string
        self.histogram=CounterDictionary()
        self._parser()
        self._cleanup()
        self.number_symbols=self._count_symbols()

    def _skip_operator_name(self, i, n):
        """ i : current index
            n : lenght string
            output tuple: new index, name of the operator"""
        it=i
        name=[]
        while it<n-1 and self.equation_string[it+1] not in OPERATOR_STOPPERS:
            it+=1
            name.append(self.equation_string[it])
        return it, "".join(name)

    def _parser(self):
        """ parse equation updates count and updates list of different symbols"""
        n=len(self.equation_string)
        i=0
        while i<n:
            symbol=self.equation_string[i]
            if symbol in SYMBOLS_TO_SKIP:
                pass
            elif symbol=="\\":
                i ,name =self._skip_operator_name(i, n)
                if name in SYMBOLS_TO_CHECK_IN:
                    i+=1#skip the first {
                    i, name=self._skip_operator_name(i,n)
                self.histogram.add(name)
            else:
                self.histogram.add(symbol)
            i+=1

    def _cleanup(self):
        for name in SYMBOLS_TO_REMOVE:
            self.histogram.pop(name)

    def _count_symbols(self):
        return sum(self.histogram.values())
                  
    def get_histogram(self):
        return self.histogram.D
    
