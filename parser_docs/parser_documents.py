from doctest import testfile
import os
import time
from warnings import warn

#load dummy data
# self_dir = os.path.dirname(os.path.realpath(__file__))
# with open(self_dir + "/test_document_dirty.tex") as test_file:
#         test_file = test_file.read()  

operator_stoppers=["!","^","*","-","+","/","<",">","=","(",")","[","]",r"\ "[0], " ",
                   "1","2","3","4","5","6","7","8","9","0",",","_","{","}", "&","$","|", ".",",", ":", ";","'","`","\n"]

def removeLeadingSpaces(s):
    #print("ALIVE")
    while s[0] == " ":
        #print("ALIVE")
        s= s[1:]
    return s

def balanceCharacters(s,L=r"{",R=r"}"):
    #print(s)
    #print("ALIVE2")
    while s.count(L)>s.count(R):
        s = s + R   
    #print("ALIVE2")                 
    while s.count(L)<s.count(R):
        s = R + s 
    #print(s)
    return s

def removeCustomCommands(tex_text=r"\begin{document}$dummy eq$\end{document}",verbose = 0):
    try:
        preamble = tex_text[0:tex_text.index(r"\begin{document}")]
        preamble = preamble.replace(r"{{",r"{")
        preamble = preamble.replace(r"}}",r"}")
    except:
        warn("Couldn't find document beginning or document end!")
        
    
    #find and replace defs
    
    replace_defs =[]
    s_index = 0
    if verbose>0:
        print("Finding defs...")   
        start_time = time.time()
    while True:
        try:
                #find defs
                s_index = 4 +  preamble.index(r"\def",s_index)                 
                e_index = preamble.index(r"{",s_index) 
                find_s = preamble[s_index:e_index]
                s_index = e_index + 1
                e_index = preamble.index(r"}",s_index) 
                replace_s = preamble[s_index:e_index]

                #balance brackets
                replace_s = balanceCharacters(removeLeadingSpaces(replace_s))
                find_s = balanceCharacters(removeLeadingSpaces(find_s))               

                #save commands
                replace_defs.append([find_s,replace_s])
                             
        except:             
            break

    if verbose>0:
        print("Defs: " +str(len(replace_defs)))
        print("Replacing...")

    for d in replace_defs:
        if verbose > 1:
                    print(d[0] + " => " +d[1])
        for stop in operator_stoppers:                    
            tex_text = tex_text.replace(d[0]+stop,d[1]+stop)    

    if verbose > 0:
        print("Done")    
        print("%s seconds" % (time.time() - start_time))    
        print("--------------------")
    
    #find and replace commands

    replace_coms =[]
    s_index = 0
    if verbose>0:
        print("Finding commands...")   
        start_time = time.time()

    while True:
        try:
                #print("HEY!")
                #find commands
                s_index = 12 +  preamble.index(r"\newcommand",s_index)                 
                e_index = preamble.index(r"}",s_index+1) 
                find_s = preamble[s_index:e_index]
                s_index = e_index + 2
                e_index = preamble.index(r"}",s_index) 
                replace_s = preamble[s_index:e_index]
                #print("HEY!")
                #balance brackets
                replace_s = balanceCharacters(removeLeadingSpaces(replace_s))
                find_s = balanceCharacters(removeLeadingSpaces(find_s)) 

                #save commands
                replace_coms.append([find_s,replace_s])
                #print("HEY!")
                
        except:             
            break

    if verbose>0:
        print("Commands: " +str(len(replace_coms)))
        print("Replacing...")

    for c in replace_coms:
        if verbose > 1:
                    print(c[0] + " => " + c[1])
        for stop in operator_stoppers:
            tex_text = tex_text.replace(c[0]+stop,c[1]+stop)    

    if verbose > 0:
        print("Done")
        print("%s seconds" % (time.time() - start_time))        
        print("--------------------") 
   
    return tex_text   

def getEquations(tex_text=r"\begin{document}$dummy eq$\end{document}", verbose = 0):

    #Get main text    
    
    try:
        tex_text = tex_text[tex_text.index(r"\begin{document}")+16:tex_text.index(r"\end{document}")]
    except:
        warn("Couldn't find document beginning or document end!")

    #Load math mode delimiters    

    self_dir = os.path.dirname(os.path.realpath(__file__))

    with open(self_dir+ '/enter_math_mode.csv', "r") as f:
        math_starts = f.read()
        math_starts = math_starts.split("\n")

    math_asterisk =[]

    for i in math_starts:
        math_asterisk.append(i.replace("}","*}",1))

    math_starts = math_starts + math_asterisk

    math_ends = []
    for i in math_starts:
        math_ends.append(i.replace("begin","end"))

    math_starts.append(r"\(")
    math_starts.append(r"\[")
    math_starts.append(r"$$")
    math_starts.append(r"$")

    math_ends.append(r"\)")
    math_ends.append(r"\]")
    math_ends.append(r"$$")
    math_ends.append(r"$")

    #Find equations

    equations = []
    equation_locs = []
    if verbose >0:
        print("\nStarting.... \nInput Characters: " + str(len(tex_text)))
        print("--------------------")
        start_time = time.time()
    for i in range(len(math_starts)):    
        if verbose >0:
            print("Search: " + math_starts[i])
        
        s_index = 0   
        while True:
            try:
                    s_index = len(math_starts[i]) + tex_text.index(math_starts[i],s_index) 
                    e_index = tex_text.index(math_ends[i],s_index)
                    equations.append(tex_text[s_index:e_index])
                    equation_locs.append([s_index,e_index])
                    s_index = e_index + len(math_ends[i])
                 
            except: 
                if verbose >0:
                    print("Done")
                    print("%s seconds" % (time.time() - start_time))
                    print("--------------------")
                break
    
    #Remove equations nested in other equations
    
    start_time = time.time()
    clean_equations = []    
    for i in range(len(equations)):
        found = False        
        if verbose>0:
            if i%1000==0:
                print(str(i+1) +" of " + str(len(equations))) 
                pass 
        for j in range(len(equations)):
                if equation_locs[i][0]>equation_locs[j][0]: 
                    if equation_locs[i][1]<equation_locs[j][1]:
                        found = True
                        break  
        if not found:
            clean_equations.append(equations[i])
    if verbose>0:
        print("%s seconds" % (time.time() - start_time))
       
    #Report equations
    if verbose>0:          
        print("Equations: " + str(len(clean_equations)))

    if verbose>1:
        for i in clean_equations:
            if len(i)>200:
                print(i)


    return clean_equations

# test_file = removeCustomCommands(test_file,2)
# getEquations(test_file, 1)
