import os
import xml.etree.ElementTree as ET
import time
import sys
from urllib.error import HTTPError
import requests
import gzip
import pandas as pd
import random 
import numpy as np
import csv
import re
from  bs4 import BeautifulSoup

OAI = '{http://www.openarchives.org/OAI/2.0/}'
BASE = 'http://export.arxiv.org/oai2?verb=ListRecords&'

categories = ['math']

subcategories = {'math':['math.AG', 'math.AT', 'math.AP', 'math.CT', 'math.CA', 'math.CO',
		  'math.AC', 'math.CV', 'math.DG', 'math.DS', 'math.FA', 'math.GM', 'math.GN',
		  'math.GT', 'math.GR', 'math.HO', 'math.IT', 'math.KT', 'math.LO', 'math.MP',
		  'math.MG', 'math.NT', 'math.NA', 'math.OA', 'math.OC', 'math.PR', 'math.QA',
		  'math.RT', 'math.RA', 'math.SP', 'math.ST', 'math.SG']}
		  
class Scraper():
	"""
	A class to hold info about attributes of scraped records
	Paramters
	---------
	category: str
		The category of scraped records
	data_from: str
		starting date in format 'YYYY-MM-DD'. Updated eprints are included even if
		they were created outside of the given date range. Default: first day of current month.
	t: int
		Waiting time between subsequent calls to API, triggred by Error 503.
	"""

	def __init__(self, category, date_from=None, date_until=None, t=30):
		self.cat = str(category)
		self.t = t
		self.f = date_from
		self.url = 'https://arxiv.org/search/advanced?advanced=&terms-0-operator=AND&terms-0-term=&terms-0-field=title&classification-mathematics=y&classification-physics_archives=all&classification-include_cross_list=exclude&date-year=&date-filter_by=date_range&date-from_date={}&date-to_date={}&date-date_type=submitted_date&abstracts=hide&size=200&order=-announced_date_first'.format(date_from,date_until)

	def _get_source(self, target_url):
		
		# dl source (not metadata)
		response = requests.get(target_url)
		
		# catchall timeout
		if not response.ok:
			print('\n',response.status_code,target_url)
			time.sleep(5)
			return False
		
		# source files may be compressed
		try:
			data = gzip.decompress(response.content)
		except:
			data = response.content
		
		return data 

	def scrape(self, k):
		url = self.url
		ds = []
		i = 0

		print('fetching up to ', k, 'records from',url)

		while True:
			
			# get metadata, handle timeouts
			try:
				response = requests.get(self.url)
			
			# not sure if this is even necessary any more
			except HTTPError as e:
				if e.code == 503:
					# to = int(e.hdrs.get('retry-after', 30)) # To implement this later but it is unused now
					print('Got 503. Retrying after {0:d} seconds.'.format(self.t))
					time.sleep(self.t)
					continue
				raise
			
			# get a list of individual papers
			soup = BeautifulSoup(response.text,"html.parser")
			records = soup.find_all("li", attrs={'class': 'arxiv-result'})
			
			# shuffle them so we dont sample sequentially (jan-dec)
			random.shuffle(records)
			
			for record in records:
				
				# get record in 0000.0000 format
				source = record.find_all("p", attrs={'class': 'list-title'})[0]
				try:
					source = source.find('span').find_all('a')[2].attrs['href']
				except:
					
					print('No source, continuing. Should see lots of these.')
					#'TODO accept only LaTeX'
					continue
				source = 'https://arxiv.org/e-print/math/'+source.split('/')[-1]
				# fetch source file
				source = self._get_source(source)
				
				if not source:
					# dont add record to xml if source is 404 or some other error
					continue
				else:
					source = str(source).encode('utf-8', errors='replace')
				
				# parse title (string)
				title = record.find_all(attrs={'class': 'title is-5 mathjax'})
				title = title[0].get_text()
				title = title.encode('utf-8', errors='replace')
				
				# parse (submission) date (string)
				date = record.find("span", text="Submitted").next_sibling.get_text()
				date = date.encode('utf-8', errors='replace')
				
				# parse authors (multiple entries in single string)
				authors = record.find_all("p", attrs={'class': 'authors'})[0].get_text()
				authors = authors.replace('\n','').split('Authors:')[1]
				authors = authors.encode('utf-8', errors='replace')
				
				# parse categories i.e math.CS (multiple entries in single string)
				tag = ' '.join([x.get_text() for x in record.find_all("span", attrs={'class': 'tag'})])
				tag = tag.encode('utf-8', errors='replace')
				
				# build record dict (single row/element in csv/xml)
				recdic = {
					'title': title,
					'date': date,
					'source': source,
					'authors': authors,
					'tag':tag
					 }
					 
				ds.append(recdic)
				i = i+1
				
				print('{}: {}'.format(self.f,i))
				
				# only read i papers for this year
				if i >= k:
					return ds 
			
			# not sure if this is even necessary any more
			try:
				token = root.find(OAI + 'ListRecords').find(OAI + 'resumptionToken')
			except:
				return 1
			if token is None or token.text is None:
				break
			url = BASE + 'resumptionToken=%s' % token.text


import csv

# get number of papers per year from helenas doc
preprints = pd.read_csv('preprints_counts.csv', sep = ';',header=0)
preprints = np.array(preprints,dtype = np.float)

for year,total,ns in preprints:
	
	year = int(year) # start year
	ns = np.ceil(ns) # num papers
	
	# don't download data we already have
	if not os.path.isfile('{}_math.xml'.format(year)):
		
		# scrape one year
		scraper = Scraper(category='math', date_from=str(year),date_until=str(year+1))
		records = scraper.scrape(ns)
		
		# convert to dataframe
		df = pd.DataFrame(records, columns=['title', 'date', 'source', 'authors', 'tag'])
		# write csv
		df.to_csv('{}_math.csv'.format(year), index = None, header=True) 
		# write xml
		xml = df.to_xml()
		f =	 open("{}_math.xml".format(year), "w")
		f.write(xml)
		f.close()		