import xml.etree.ElementTree as ET
import os

xlm_file=("1991_math.xml",  "1995_math.xml",  "1999_math.xml",  "2003_math.xml",\
          "1992_math.xml",  "1996_math.xml",  "2000_math.xml",  "2004_math.xml",\
          "1993_math.xml",  "1997_math.xml",  "2001_math.xml",  "2005_math.xml",
          "1994_math.xml",  "1998_math.xml",  "2002_math.xml")
for inputfilename in xlm_file:
    fil=open("1991_math.xml",'rt',encoding='utf-8')
    data=fil.read()
    data.encode('utf-8')
    tree=ET.fromstring(data)
    #tree=ET.parse("1991_math.xml")
    root=tree.getroot()
    print(root)
    number_files=len(root)
    self_dir = os.path.dirname(os.path.realpath(__file__))
    for i in range(len(root)):
        index=root[i][1].text
        title=root[i][2].text
        date=root[i][3].text
        source=root[i][4].text
        authors=root[i][5].text
        category=root[i][5].text
        filename=index+";"+date+";"+authors+";"+category+";.tex"
        filename=filename.replace(" ",",")
        with open("/home/alessio/SantaFe/72hours/parser_docs/all_tex_files/"+filename,"w") as output:
             output.write(source)
    fil.close()



# parser = ETree.XMLParser(encoding="utf-8")
# tree = ETree.parse("test_xml.xml", parser=parser)
